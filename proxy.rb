require 'rubygems'
require 'sinatra'
require 'json'
require 'base64'
require 'mechanize'
require 'mechanize-random-agent'
require 'pry' if ENV["DEBUG"] = "true"

$next_usage = {}
$mutex = {}
th_change = (ENV["TH_CHANGE_AGENT"] || 100).to_i

def wait_for(host)
  return if $next_usage[host].nil?
  t = $next_usage[host][:time].to_i
  if t && t > Time.now.to_i
    duration = t - Time.now.to_i
    #puts "Waits #{duration} seconds"
    sleep(duration)
  end
end

get '/usable' do
  return $next_usage.to_json
end

get '/usable/reset/:host' do
  host = params[:host]
  $next_usage[host] = nil
  return $next_usage.to_json
end

get '/' do
  if params[:url].nil?
    status 404
    return "r y t3r ?"
  end
  url = Base64.strict_decode64(params[:url])
  # capture the current host and mutex
  host = URI.parse(url).host
  $mutex[host] ||= Mutex.new

  # prepare how much time the proxy will wait before retry this host
  wait = 1
  if ENV["PROXY_RAND_WAIT_USE"] == "true"
    min = (ENV["PROXY_RAND_MIN_WAIT"] || 1).to_i
    max = (ENV["PROXY_RAND_MAX_WAIT"] || 3).to_i
    wait = rand(min..max)
  elsif ENV["PROXY_MIN_WAIT"]
    wait = ENV["PROXY_MIN_WAIT"].to_i
  end

  begin
    # while cannot aquire the lock, sleep
    while $mutex[host].try_lock == false
      wait_for host
    end
    # the thread has the lock
    wait_for host

    $agent ||= Mechanize.new.random_user_agent
    $agent = Mechanize.new.random_user_agent if rand(1..th_change) == 1

    tStart = Time.now
    # try to get the data
    begin
      page = $agent.get(url)
      @ret = page.content
    rescue => err
      status err.response_code.to_i
      @ret = err.message
    end
    tEnd = Time.now
    duration = tEnd - tStart

    # update the sleep
    sleep_duration = (duration * (ENV["PROXY_MULTI_WAIT"] || 1).to_i).to_i + wait
    sleep_duration_final = [[sleep_duration, (ENV["PROXY_MIN_WAIT_GLOBAL"] || 1).to_i].max, (ENV["PROXY_MAX_WAIT_GLOBAL"] || 5).to_i].min
    $next_usage[host] = {time: Time.now.to_i + sleep_duration_final, duration: sleep_duration_final}
    $mutex[host].unlock

    STDERR.puts "Request duration of\t'#{host}'\t=>\t#{duration.round(3)} s"
    STDERR.puts "Next usage of      \t'#{host}'\t=>\t'#{Time.at $next_usage[host][:time]}' \t(#{sleep_duration_final} seconds)"

    return @ret
  rescue => err
    STDERR.puts err, err.backtrace
    $mutex[host].unlock
    raise
  end
end
